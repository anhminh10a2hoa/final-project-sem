# Software Engineering Methods Final Project

## Analysis

+ Case description (one-(two) pages of text)

  + actors of the solution: <b>normal user</b>

  + roles of actors: <b>user</b>

  + tasks the actors would like to do: <b>login, sign up, comment, post and delete</b>

  + Data input: <b>username, id, post content and comment content</b>

  + Output data type: <b>JSON</b>

+ Table of functional requirements: 

  + User can login and sign up
  + User can add and delete their own posts and comments
  + User can see other posts and comments

+ Project schedule: 

  + Analysis: 9 Apr 2021
  + Design: 9 Apr 2021
  + Implementation

    + Back end: 10 Apr to 12 Apr, 2021
    + Front end: 22 Apr to 27 Apr, 2021

  + Testing: 25 Apr, 2021
  + Deployment

    + Back end: 12 Apr, 2021
    + Front end: 27 Apr, 2021

+ Use case diagram

  <img src="./useCaseDiagram.png"/>

+ Test plan
  + Get user list
  + Create user
  + Add item to database and get one from database based on that id
  + Test if user can add an user via POST
  + Test total number of comments and posts
  + Test buttons exist on the Post component

## Final products

  + Front-end repo: https://gitlab.com/anhminh10a2hoa/final-assignment-frontend
  + Front-end link: https://final-assignment-frontend.herokuapp.com
  + Swagger: https://e1800950-final.herokuapp.com/swagger-ui.html
  + Back-end repo: https://gitlab.com/hnminh/final_assignment
